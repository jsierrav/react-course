import React from 'react';


class CourseItem extends React.Component {
    render() {

    


        let buttoClass = this.props.status ? "btn btn-success" : "btn btn-danger";
        let value = this.props.status ? "Activo" : "Inactivo";

        return (

            <tr>
                <td>{this.props.name}</td>
                <td><button className={buttoClass} onClick={this.props.changeStatus.bind(this)} value={this.props.value}>{value}</button></td>
            </tr>

        )
    }
}

class CourseList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            search: "",
            courses: props.courses,
            searchOnlyActive:false
        }
    }

    setValue(event) {
        console.log(event.target.value);
        this.setState({ search: event.target.value })
    }
    changeStatus(id, event) {
        console.log(id);

        const items = this.state.courses;
        items.find((item) => item.id === id).status = event.target.value === "Inactivo";
        console.log(event.target.value === "Inactivo")
        this.setState({items})
        console.log(items);
    }

    render() {

        let col = this.state.courses.filter(
            (course) => course.name.toLowerCase().indexOf(this.state.search) !== -1
        );

        col = col.filter((course) => this.state.searchOnlyActive ? course.active : true)

        return (
            <div class="row">
                <div class="col-xs-12">
                    <input className="form-control" onChange={this.setValue.bind(this)} type="text" value={this.state.search} />
                </div>
                <div class="col-xs-12">
                    <table class="table">
                        <tr>
                            <td>Nombre</td>
                            <td>Estado</td>

                        </tr>
                        <tbody>
                        {col.map((item) => <CourseItem key={item.id} name={item.name} status={item.status} changeStatus={this.changeStatus.bind(this,item.id)} />)}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CourseList;