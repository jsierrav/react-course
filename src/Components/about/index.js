import React from 'react';
import PropTypes from 'prop-types';
class About extends React.Component{
    render(){

    
        return (
            <div>
                <p>Nombre: {this.props.name}</p>
                <p>Edad: {this.props.age}</p>
                <p>Profesión {this.props.profession}</p>
                 <p>Skills</p>
                 {this.props.skills.map((item)=><p>id:{item.id} -{item.name}</p>)}

                 <p>Mis cursos</p>
                 {this.props.children}

                </div>
        )
    }
}

About.propTypes = {
    age:PropTypes.number.isRequired,
    name:PropTypes.string.isRequired,
    profession:PropTypes.string.isRequired,
    skills:PropTypes.array.isRequired

}

export default About;